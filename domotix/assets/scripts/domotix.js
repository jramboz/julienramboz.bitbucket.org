
/**
    The main domotix app.
    Loads dependencies and initializes the interface.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixInit;

  (function(plugin) {
    "use strict";

    /* Setting some globals */
    var global;
    global = global || window;
    global.require = (typeof require !== "undefined" ? require : false);
    if (typeof define === "function" && define.amd) {

      /* Configure requirejs and load dependencies... */
      require.config({
        baseUrl: "/assets/scripts",
        paths: {

          /* Vendor libraries */
          jslib: "../vendor/jquery/jquery",
          bootstrap: "../vendor/bootstrap/dist/js/bootstrap",

          /* Control panels */
          panel: "panels/panel",
          tempPanel: "panels/temp-panel",
          lightPanel: "panels/light-panel",
          curtainPanel: "panels/curtain-panel",

          /* Server emulation */
          server: "server/server",
          component: "server/components/component",
          temp: "server/components/temp-component",
          light: "server/components/light-component",
          curtain: "server/components/curtain-component"
        },
        map: {
          "*": {
            "jquery": "jslib"
          }
        },
        shim: {
          jslib: {
            exports: "$"
          }
        }
      });
      return require(["jslib", "bootstrap", "server", "tempPanel", "lightPanel", "curtainPanel"], plugin);
    } else {

      /* ... or fall back to standard loading */
      return plugin(this.$, null, this.server, this.DomotixTempPanel, this.DomotixLightPanel, this.DomotixCurtainPanel);
    }
  })(domotixInit = function($, _, server, DomotixTempPanel, DomotixLightPanel, DomotixCurtainPanel) {
    "use strict";

    /* Defining the class for the client */
    var DomotixApp, client, hash, navbar, p, r, room, section, _i, _j, _len, _len1, _ref, _ref1;
    DomotixApp = (function() {

      /*
          The default constructor
      
          - `s` [DomotixServer]: the server instance to connect to
       */
      function DomotixApp(s) {
        var Renderer, c, panel, _i, _len, _ref;
        this.server = s;
        this.panels = [];

        /* Generate the panel for each component */
        panel = null;
        Renderer = null;
        _ref = this.server.getComponents();
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          c = _ref[_i];
          Renderer = DomotixApp.panelRenderers[c.getType()];
          if (Renderer) {
            panel = new Renderer(c, server);
            this.panels.push(panel);
          }
        }
      }


      /* Get the panels for the components */

      DomotixApp.prototype.getPanels = function() {
        return this.panels;
      };


      /*
          Show the panels for the specified room.
      
          - `room` [String]: the room the components are in
       */

      DomotixApp.prototype.showPanels = function(room) {
        $("[data-room]").hide();
        return $("[data-room=\"" + room + "\"]").show();
      };


      /*
          Trigger event for all components of a type.
      
          - `t` [String]: the component type
          - `e` [String]: the event to trigger
       */

      DomotixApp.prototype.triggerAll = function(t, e) {
        var cb, p, panels, _i, _len, _results;
        panels = this.panels.filter(function(p) {
          return p.component.getType() === t;
        });
        cb = function(c) {
          return p.updateComponent(c);
        };
        _results = [];
        for (_i = 0, _len = panels.length; _i < _len; _i++) {
          p = panels[_i];
          _results.push(this.server.triggerEvent(p.component.getId(), e, cb));
        }
        return _results;
      };


      /* Define renderers */

      DomotixApp.panelRenderers = {
        temperature: DomotixTempPanel,
        curtain: DomotixCurtainPanel,
        light: DomotixLightPanel
      };

      return DomotixApp;

    })();

    /* Initializing a new client instance. */
    client = new DomotixApp(server);

    /* Add rooms navigation */
    section = $("#content");
    navbar = $(".navbar .navbar-nav");
    room = "";
    _ref = server.getRooms();
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      r = _ref[_i];
      room = r.toLowerCase().replace(/[^\w]/, "-");
      navbar.append($("<li><a href=\"#" + room + "\">" + r + "</a></li>"));
      section.append($("<section id=\"" + room + "\" data-room=\"" + room + "\" style=\"display:none\"><header><h1 class=\"page-header\">" + r + "</h1></header><div id=\"" + room + "-section\" class=\"row\"></div></section>"));
    }

    /* Handle rooms filtering */
    navbar.on("click", "a", (function(_this) {
      return function(e) {
        window.location.hash = "#" + (e.target.href ? e.target.href.split('#')[1] : "");
        $("li", navbar).removeClass("active");
        $(e.target.parentNode).addClass("active");
        client.showPanels(e.target.href ? e.target.href.split('#')[1] : "");
        return $(".navbar-collapse").collapse("hide");
      };
    })(this));

    /* Pre-filter the room if specifed in the URL */
    if (window.location.hash) {
      hash = window.location.hash;
      client.showPanels(hash ? hash.substring(1) : "");
    }

    /* Add the panels to the page */
    section = null;
    _ref1 = client.getPanels();
    for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
      p = _ref1[_j];
      section = $("#" + (p.component.getRoom().toLowerCase().replace(/[^\w]/, "-")) + "-section");
      section.append(p.getHTML());
    }

    /* Handle global controllers */
    $("#general-temperature-section").on("click", "[type=\"submit\"], .btn .icon", (function(_this) {
      return function(e) {
        var $target, op;
        e.preventDefault();
        e.stopPropagation();
        $target = $(e.target);
        if ($target.is(".icon")) {
          $target = $target.parent(".btn");
        }
        op = $target.val();
        $("#general-temperature-section .btn").removeClass().addClass("btn btn-default");
        switch (op) {
          case "day":
            client.triggerAll("temperature", "switchDayMode");
            return $target.toggleClass("btn-default btn-success");
          case "night":
            client.triggerAll("temperature", "switchNightMode");
            return $target.toggleClass("btn-default btn-primary");
          case "off":
            client.triggerAll("temperature", "switchOff");
            return $target.toggleClass("btn-default btn-danger");
        }
      };
    })(this));
    $("#general-curtain-section").on("change", "input[type='checkbox']", (function(_this) {
      return function(e, data) {
        return client.triggerAll("curtain", e.target.checked ? "open" : "close");
      };
    })(this));
    $("#general-light-section").on("change", "input[type='checkbox']", (function(_this) {
      return function(e, data) {
        return client.triggerAll("light", e.target.checked ? "switchOn" : "switchOff");
      };
    })(this));
    return client;
  });

}).call(this);
