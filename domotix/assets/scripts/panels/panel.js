
/**
    The panel class provides the prototype for all other panels.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixPanel;

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib"], plugin);
    } else {
      return plugin(this.$);
    }
  })(domotixPanel = function($) {
    "use strict";

    /* Defining the class */
    var DomotixPanel;
    DomotixPanel = (function() {

      /*
          The default class constructor
      
          - `component` [Object]: the component for the panel
          - `server` [Object]: the server instance to forward the events to
       */
      function DomotixPanel(component, server) {
        var $heading, $panel;
        this.component = component;
        this.server = server;
        this.wrapper = $(document.createElement("div")).attr({
          "data-room": this.component.getRoom().toLowerCase().replace(/[^\w]/, "-")
        });
        this.wrapper.addClass("col-micro col-xs-6 col-sm-4 col-md-3 col-lg-2 " + (this.component.getType()) + "-controller");
        $panel = $(document.createElement("section"));
        $panel.addClass("panel panel-default");
        this.wrapper.append($panel);
        $heading = $(document.createElement("header"));
        $heading.addClass("panel-heading");
        $panel.append($heading);
        this.title = $(document.createElement("h2"));
        this.title.addClass("panel-title");
        this.title.text(this.component.getName());
        $heading.append(this.title);
        this.body = $(document.createElement("div"));
        this.body.addClass("panel-body status-" + this.component.getStatus());
        $panel.append(this.body);
        this.body.append($(document.createElement("input")).attr({
          type: "hidden",
          name: "component",
          value: this.id
        }));
        this.fillPanelBody();
        this.handleEvents();
      }


      /* Register event handlers */

      DomotixPanel.prototype.handleEvents = function() {};


      /* Handle button events */

      DomotixPanel.prototype.handleEvents = function(eventMap) {
        return $(this.body).on("click", "[type=\"submit\"], .btn .icon", (function(_this) {
          return function(e) {
            var $target, op, reload;
            e.preventDefault();
            e.stopPropagation();
            $target = $(e.target);
            if ($target.is(".icon")) {
              $target = $target.parent(".btn");
            }
            op = $target.val();
            reload = function(c, err) {
              if (err) {
                return console.err(err);
              }
              return _this.updateComponent(c);
            };
            switch (op) {
              case "+":
                return _this.server.triggerEvent(_this.component.id, "increaseTemp", reload);
              case "–":
                return _this.server.triggerEvent(_this.component.id, "decreaseTemp", reload);
              case "day":
                return _this.server.triggerEvent(_this.component.id, "switchDayMode", reload);
              case "night":
                return _this.server.triggerEvent(_this.component.id, "switchNightMode", reload);
              case "off":
                return _this.server.triggerEvent(_this.component.id, "switchOff", reload);
            }
          };
        })(this));
      };


      /*
          Update the component
      
          - `c` [Object]: the new component for the panel
       */

      DomotixPanel.prototype.updateComponent = function(c) {
        this.component = c;
        this.updatePanel();
        return this.component;
      };


      /* Update the panel */

      DomotixPanel.prototype.updatePanel = function() {
        this.title.text(this.component.getName());
        this.body.removeClass().addClass("panel-body status-" + this.component.getStatus());
        this.body.empty();
        return this.fillPanelBody();
      };


      /* Fill the panel body for the specific component */

      DomotixPanel.prototype.fillPanelBody = function() {
        return this.wrapper;
      };


      /* Get the HTML for the panel */

      DomotixPanel.prototype.getHTML = function() {
        return this.wrapper;
      };


      /*
          Build the toggle swtich for the panel
      
          - `activeValue`: activate the switch if the component status has this value
       */

      DomotixPanel.prototype.buildToggleSwitch = function(activeValue) {
        var $toggleLabel, $toggleSwitch;
        $toggleSwitch = $(document.createElement("input")).attr({
          id: "" + (this.component.getId()) + "-switch",
          type: "checkbox"
        });
        if (this.component.getStatus() === activeValue) {
          $toggleSwitch.attr("checked", "checked");
        }
        this.body.append($toggleSwitch);
        $toggleLabel = $(document.createElement("label")).attr({
          "class": "btn btn-default",
          "for": "" + (this.component.getId()) + "-switch",
          title: "Change status"
        });
        $toggleLabel.html("<span class=\"sr-only\">Status</span>");
        this.body.append($toggleLabel);
        return $toggleLabel;
      };


      /* Build a simple label for the panel */

      DomotixPanel.prototype.buildLabel = function() {
        var $label;
        $label = $(document.createElement("div")).addClass("panel-label");
        this.body.append($label);
        return $label;
      };


      /* Build the icon for the panel */

      DomotixPanel.prototype.buildIconPreview = function() {
        var $info;
        $info = $(document.createElement("div")).addClass("well");
        this.body.append($info);
        return $info;
      };


      /* Build a button group for the panel */

      DomotixPanel.prototype.buildButtonGroup = function() {
        var $group;
        $group = $(document.createElement("div")).addClass("btn-group");
        this.body.append($group);
        return $group;
      };


      /*
          Build an input button for the panel
      
          - `group` [Object]: the group the button belongs to
       */

      DomotixPanel.prototype.buildInputButton = function($group) {
        var $button;
        $button = $(document.createElement("input")).attr({
          "class": "btn btn-default",
          type: "submit"
        });
        $group.append($button);
        return $button;
      };


      /*
          Build a button for the panel
      
          - `group` [Object]: the group the button belongs to
       */

      DomotixPanel.prototype.buildButton = function($group) {
        var $button;
        $button = $(document.createElement("button")).attr({
          "class": "btn btn-default",
          type: "submit"
        });
        $group.append($button);
        return $button;
      };


      /*
          Build a label-button for the panel
      
          - `group` [Object]: the group the label belongs to
       */

      DomotixPanel.prototype.buildLabelButton = function($group) {
        var $label;
        $label = $(document.createElement("span")).addClass("btn btn-label");
        $group.append($label);
        return $label;
      };

      return DomotixPanel;

    })();

    /* Return the class */
    return DomotixPanel;
  });

}).call(this);
