
/**
    The curtain panel class that renders the curtain control panel.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixCurtainPanel,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib", "panel"], plugin);
    } else {
      return plugin(this.$, this.DomotixPanel);
    }
  })(domotixCurtainPanel = function($, DomotixPanel) {
    "use strict";

    /* Defining the class */
    var DomotixCurtainPanel;
    DomotixCurtainPanel = (function(_super) {
      __extends(DomotixCurtainPanel, _super);

      function DomotixCurtainPanel() {
        return DomotixCurtainPanel.__super__.constructor.apply(this, arguments);
      }


      /* Register event handlers */

      DomotixCurtainPanel.prototype.handleEvents = function() {

        /* Handle clicks on switches */
        $(this.body).on("change", "input[type='checkbox']", (function(_this) {
          return function(e, data) {
            var $target, checked, reload;
            $target = $(e.target);
            checked = e.target.checked;
            reload = function(c, err) {
              if (err) {
                return console.err(err);
              }
              return _this.updateComponent(c);
            };
            return _this.server.triggerEvent(_this.component.id, (checked ? "open" : "close"), reload);
          };
        })(this));

        /* Handle clicks on other buttons */
        return $(this.body).on("click", "[type=\"submit\"], .btn .icon", (function(_this) {
          return function(e) {
            var $target, op, reload;
            e.preventDefault();
            e.stopPropagation();
            $target = $(e.target);
            if ($target.is(".icon")) {
              $target = $target.parent(".btn");
            }
            op = $target.val();
            reload = function(c, err) {
              if (err) {
                return console.err(err);
              }
              return _this.updateComponent(c);
            };
            switch (op) {
              case "+":
                return _this.server.triggerEvent(_this.component.id, "increaseIntensity", reload);
              case "–":
                return _this.server.triggerEvent(_this.component.id, "decreaseIntensity", reload);
            }
          };
        })(this));
      };


      /* Fill the panel body for the specific component */

      DomotixCurtainPanel.prototype.fillPanelBody = function() {

        /* The open/close switch */
        this.buildToggleSwitch("open").attr({
          "data-checked-label": "open",
          "data-unchecked-label": "closed"
        });

        /* Add the current label */
        this.buildLabel().text("Current");

        /* The icon for the panel */
        this.buildIconPreview().addClass(this.component.getStatus() === "open" ? "open" : "closed");
        return this.body;
      };

      return DomotixCurtainPanel;

    })(DomotixPanel);

    /* Return the class */
    return DomotixCurtainPanel;
  });

}).call(this);
