
/**
    The light panel class that renders the light control panel.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixLightPanel,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib", "panel"], plugin);
    } else {
      return plugin(this.$, this.DomotixPanel);
    }
  })(domotixLightPanel = function($, DomotixPanel) {
    "use strict";

    /* Defining the class */
    var DomotixLightPanel;
    DomotixLightPanel = (function(_super) {
      __extends(DomotixLightPanel, _super);

      function DomotixLightPanel() {
        return DomotixLightPanel.__super__.constructor.apply(this, arguments);
      }


      /* Register event handlers */

      DomotixLightPanel.prototype.handleEvents = function() {

        /* Handle clicks on switches */
        $(this.body).on("change", "input[type='checkbox']", (function(_this) {
          return function(e, data) {
            var $target, checked, reload;
            $target = $(e.target);
            checked = e.target.checked;
            reload = function(c, err) {
              if (err) {
                return console.err(err);
              }
              return _this.updateComponent(c);
            };
            return _this.server.triggerEvent(_this.component.id, (checked ? "switchOn" : "switchOff"), reload);
          };
        })(this));

        /* Handle clicks on other buttons */
        return $(this.body).on("click", "[type=\"submit\"], .btn .icon", (function(_this) {
          return function(e) {
            var $target, op, reload;
            e.preventDefault();
            e.stopPropagation();
            $target = $(e.target);
            if ($target.is(".icon")) {
              $target = $target.parent(".btn");
            }
            op = $target.val();
            reload = function(c, err) {
              if (err) {
                return console.err(err);
              }
              return _this.updateComponent(c);
            };
            switch (op) {
              case "+":
                return _this.server.triggerEvent(_this.component.id, "increaseIntensity", reload);
              case "–":
                return _this.server.triggerEvent(_this.component.id, "decreaseIntensity", reload);
            }
          };
        })(this));
      };


      /* Fill the panel body for the specific component */

      DomotixLightPanel.prototype.fillPanelBody = function() {

        /* The on/off switch */
        var $button, $group;
        this.buildToggleSwitch("on").attr({
          "data-checked-label": "on",
          "data-unchecked-label": "off"
        });

        /* Add the current label */
        this.buildLabel().text("Current");

        /* The icon for the panel */
        this.buildIconPreview().text(this.component.getStatus() === "on" ? this.component.getCurrentIntensity() + "%" : "0%");

        /* Add the adjust label */
        this.buildLabel().text("Adjust");

        /* The light intensity adjuster */
        $group = this.buildButtonGroup().addClass("control-adjust");
        $button = this.buildInputButton($group).attr({
          title: "Dim light",
          value: "–"
        });
        if (this.component.getIntensity() <= this.component.options.minIntensity) {
          $button.attr("disabled", "disabled");
        }
        this.buildLabelButton($group).text(this.component.getIntensity() + "%");
        $button = this.buildInputButton($group).attr({
          title: "Brighten light",
          value: "+"
        });
        if (this.component.getIntensity() >= this.component.options.maxIntensity) {
          $button.attr("disabled", "disabled");
        }
        return this.body;
      };

      return DomotixLightPanel;

    })(DomotixPanel);

    /* Return the class */
    return DomotixLightPanel;
  });

}).call(this);
