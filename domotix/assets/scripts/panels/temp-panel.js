
/**
    The temperature panel class that renders the temperature control panel.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixTempPanel,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib", "panel"], plugin);
    } else {
      return plugin(this.$, this.DomotixPanel);
    }
  })(domotixTempPanel = function($, DomotixPanel) {
    "use strict";

    /* Defining the class */
    var DomotixTempPanel;
    DomotixTempPanel = (function(_super) {
      __extends(DomotixTempPanel, _super);

      function DomotixTempPanel() {
        return DomotixTempPanel.__super__.constructor.apply(this, arguments);
      }


      /* Register event handlers */

      DomotixTempPanel.prototype.handleEvents = function() {

        /* Handle clicks on buttons */
        return $(this.body).on("click", "[type=\"submit\"], .btn .icon", (function(_this) {
          return function(e) {
            var $target, op, reload;
            e.preventDefault();
            e.stopPropagation();
            $target = $(e.target);
            if ($target.is(".icon")) {
              $target = $target.parent(".btn");
            }
            op = $target.val();
            reload = function(c, err) {
              if (err) {
                return console.err(err);
              }
              return _this.updateComponent(c);
            };
            switch (op) {
              case "+":
                return _this.server.triggerEvent(_this.component.id, "increaseTemp", reload);
              case "–":
                return _this.server.triggerEvent(_this.component.id, "decreaseTemp", reload);
              case "day":
                return _this.server.triggerEvent(_this.component.id, "switchDayMode", reload);
              case "night":
                return _this.server.triggerEvent(_this.component.id, "switchNightMode", reload);
              case "off":
                return _this.server.triggerEvent(_this.component.id, "switchOff", reload);
            }
          };
        })(this));
      };


      /* Fill the panel body for the specific component */

      DomotixTempPanel.prototype.fillPanelBody = function() {

        /* The mode chooser */
        var $button, $group;
        $group = this.buildButtonGroup().addClass("control-mode");
        $button = this.buildButton($group).attr({
          title: "Enable day mode",
          value: "day"
        }).html("<span class=\"icon icon-sun\"></span><span class=\"sr-only\">Day mode</span>");
        if (this.component.getStatus() === "day") {
          $button.toggleClass("btn-default btn-success");
        }
        $button = this.buildButton($group).attr({
          title: "Enable night mode",
          value: "night"
        }).html("<span class=\"icon icon-moon\"></span><span class=\"sr-only\">Night mode</span>");
        if (this.component.getStatus() === "night") {
          $button.toggleClass("btn-default btn-primary");
        }
        $button = this.buildButton($group).attr({
          title: "Turn off",
          value: "off"
        }).html("<span class=\"sr-only\">Switch</span>OFF");
        if (this.component.getStatus() === "off") {
          $button.toggleClass("btn-default btn-danger");
        }

        /* Add the current label */
        this.buildLabel().text("Current");

        /* The icon for the panel */
        this.buildIconPreview().text(this.component.getCurrentTemp() + "°");

        /* Add the adjust label */
        this.buildLabel().text("Adjust");

        /* The temperature adjuster */
        $group = this.buildButtonGroup().addClass("control-adjust");
        $button = this.buildInputButton($group).attr({
          title: "Decrease temperature",
          value: "–"
        });
        if (this.component.getTemp() <= this.component.options.minTemp) {
          $button.attr("disabled", "disabled");
        }
        this.buildLabelButton($group).text(this.component.getTemp() + "°");
        $button = this.buildInputButton($group).attr({
          title: "Increase temperature",
          value: "+"
        });
        if (this.component.getTemp() >= this.component.options.maxTemp) {
          $button.attr("disabled", "disabled");
        }
        return this.body;
      };

      return DomotixTempPanel;

    })(DomotixPanel);

    /* Return the class */
    return DomotixTempPanel;
  });

}).call(this);
