
/**
    The server class emulating the actual server.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixServer;

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define("server", ["jslib", "component", "temp", "light", "curtain"], plugin);
    } else {
      return plugin(this.$, this.DomotixComponent, this.DomotixTempComponent, this.DomotixLightComponent, this.DomotixCurtainComponent);
    }
  })(domotixServer = function($, DomotixComponent, DomotixTempComponent, DomotixLightComponent, DomotixCurtainComponent) {
    "use strict";

    /* Defining the class */
    var DomotixServer, bed_room, living_room, server;
    DomotixServer = (function() {

      /* Default constructor */
      function DomotixServer() {
        this.components = {};
        this.rooms = [];
      }


      /*
          Add a new component
      
          - `c` [Object]: the component to add
       */

      DomotixServer.prototype.addComponent = function(c) {
        if (!c instanceof DomotixComponent) {
          return console.err("invalid component type", c);
        }
        console.log("[init] Component: ", c);
        this.components[c.id] = c;
        return c;
      };


      /*
          Get a specific component
      
          - `id` [String]: the component id to look for
       */

      DomotixServer.prototype.getComponent = function(id) {
        return this.components[id];
      };


      /* Get the components */

      DomotixServer.prototype.getComponents = function() {
        var c, components, id, _ref;
        components = [];
        _ref = this.components;
        for (id in _ref) {
          c = _ref[id];
          components.push(c);
        }
        return components;
      };


      /*
          Add a new room
      
          - `r` [String]: the room to add
       */

      DomotixServer.prototype.addRoom = function(r) {
        console.log("[init] Room: ", r);
        this.rooms.push(r);
        return r;
      };


      /* Get the rooms */

      DomotixServer.prototype.getRooms = function() {
        return this.rooms;
      };


      /*
          Trigger an event for a specific component
      
          - `id` [String]: the component id
          - `e` [String]: the event to trigger
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixServer.prototype.triggerEvent = function(id, e, cb) {
        var component, method;
        component = this.components[id];
        console.log("[event]", id, e, component);
        if (!component) {
          return cb(null, "unknown component");
        }
        method = component[e];
        if (!method) {
          return cb(null, "unknown event");
        }
        return method.call(component, cb);
      };

      return DomotixServer;

    })();

    /* initialize a new server instance */
    server = new DomotixServer();

    /* Components for "Living room" */
    living_room = "Living room";
    server.addRoom(living_room);
    server.addComponent(new DomotixTempComponent("comp1", "Heater", living_room, {
      defaultTemp: 21,
      defaultStatus: "day"
    }));
    server.addComponent(new DomotixLightComponent("comp2", "Ceiling light", living_room));
    server.addComponent(new DomotixLightComponent("comp3", "Couch lamp", living_room));
    server.addComponent(new DomotixCurtainComponent("comp4", "Curtains", living_room));

    /* Components for "Bed room" */
    bed_room = "Bed room";
    server.addRoom(bed_room);
    server.addComponent(new DomotixTempComponent("comp5", "Heater", bed_room, {
      defaultTemp: 19,
      defaultStatus: "night"
    }));
    server.addComponent(new DomotixLightComponent("comp6", "Ceiling light", bed_room));
    server.addComponent(new DomotixLightComponent("comp7", "Bedside lamp 1", bed_room, {
      defaultIntensity: 60
    }));
    server.addComponent(new DomotixLightComponent("comp8", "Bedside lamp 2", bed_room, {
      defaultIntensity: 60
    }));
    server.addComponent(new DomotixCurtainComponent("comp9", "Curtains", bed_room));

    /* Return the server instance */
    return server;
  });

}).call(this);
