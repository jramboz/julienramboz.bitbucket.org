
/**
    The curtain component class that represents the curtain controller.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixCurtainComponent,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib", "component"], plugin);
    } else {
      return plugin(this.$, this.DomotixComponent);
    }
  })(domotixCurtainComponent = function($, DomotixComponent) {
    "use strict";

    /* Defining the class */
    var DomotixCurtainComponent;
    DomotixCurtainComponent = (function(_super) {
      __extends(DomotixCurtainComponent, _super);


      /*
          Default constructor
      
          - `id` [String]: the component id
          - `name` [String]: the name that will be displayed for this component
          - `room` [String]: the room where the component is
          - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
       */

      function DomotixCurtainComponent(id, name, room, options) {
        if (name == null) {
          name = "";
        }
        DomotixCurtainComponent.__super__.constructor.call(this, id, name, room, "curtain", $.extend({
          defaultStatus: "open"
        }, options));
      }


      /*
          Set the component status after validation
      
          - `s` [String]: the new status
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixCurtainComponent.prototype.setStatus = function(s, cb) {
        s = s.toLowerCase();
        if (!s.match(/^(open|close)$/i)) {
          return cb(null, "invalid status");
        }
        return DomotixCurtainComponent.__super__.setStatus.call(this, s, cb);
      };


      /*
          Set the component status to "open"
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixCurtainComponent.prototype.open = function(cb) {
        return this.setStatus("open", cb);
      };


      /*
          Set the component status to "close"
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixCurtainComponent.prototype.close = function(cb) {
        return this.setStatus("close", cb);
      };

      return DomotixCurtainComponent;

    })(DomotixComponent);

    /* Return the class */
    return DomotixCurtainComponent;
  });

}).call(this);
