
/**
    The ligth component class that represents the light controller.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixLightComponent,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib", "component"], plugin);
    } else {
      return plugin(this.$, this.DomotixComponent);
    }
  })(domotixLightComponent = function($, DomotixComponent) {
    "use strict";

    /* Defining the class */
    var DomotixLightComponent;
    DomotixLightComponent = (function(_super) {
      __extends(DomotixLightComponent, _super);


      /*
          Default constructor
      
          - `id` [String]: the component id
          - `name` [String]: the name that will be displayed for this component
          - `room` [String]: the room where the component is
          - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
       */

      function DomotixLightComponent(id, name, room, options) {
        if (name == null) {
          name = "";
        }
        DomotixLightComponent.__super__.constructor.call(this, id, name, room, "light", $.extend({
          defaultStatus: "off",
          defaultIntensity: 80,
          minIntensity: 10,
          maxIntensity: 100,
          intensityStep: 10
        }, options));
        this.intensity = this.options.defaultIntensity;
      }


      /*
          Set the component status after validation
      
          - `s` [String]: the new status
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixLightComponent.prototype.setStatus = function(s, cb) {
        s = s.toLowerCase();
        if (!s.match(/^(on|off)$/i)) {
          return cb(null, "invalid status");
        }
        return DomotixLightComponent.__super__.setStatus.call(this, s, cb);
      };


      /*
          Set the component status to "on"
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixLightComponent.prototype.switchOn = function(cb) {
        return this.setStatus("on", cb);
      };


      /*
          Set the component status to "off"
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixLightComponent.prototype.switchOff = function(cb) {
        return this.setStatus("off", cb);
      };


      /*
          Adjust the light intensity
      
          - `i` [int]: the new intensity (0 <= i <= 100)
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixLightComponent.prototype.setIntensity = function(i, cb) {
        i = parseInt(i, 10);
        if (isNaN(i)) {
          return cb(null, "invalid intensity");
        }
        if (i < 0 || i > 100) {
          return cb(null, "invalid intensity range");
        }
        this.intensity = i;
        return cb(this);
      };


      /* Get the targeted light intensity */

      DomotixLightComponent.prototype.getIntensity = function() {
        return this.intensity;
      };


      /* Get the current light intensity */

      DomotixLightComponent.prototype.getCurrentIntensity = function() {

        /* Assume that intensity adjustments have an immediate effect */
        return this.getIntensity();
      };


      /*
          Increase the light intensity
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixLightComponent.prototype.increaseIntensity = function(cb) {
        return this.setIntensity(Math.min(this.intensity + this.options.intensityStep, this.options.maxIntensity), cb);
      };


      /*
          Decrease the light intensity
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixLightComponent.prototype.decreaseIntensity = function(cb) {
        return this.setIntensity(Math.max(this.intensity - this.options.intensityStep, this.options.minIntensity), cb);
      };

      return DomotixLightComponent;

    })(DomotixComponent);

    /* Return the class */
    return DomotixLightComponent;
  });

}).call(this);
