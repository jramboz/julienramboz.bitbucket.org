
/**
    The component class provides the prototype for all other components.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixComponent;

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib"], plugin);
    } else {
      return plugin(this.$);
    }
  })(domotixComponent = function($) {
    "use strict";

    /* Defining the class */
    var DomotixComponent;
    DomotixComponent = (function() {

      /*
          Default constructor
      
          - `id` [String]: the component id
          - `name` [String]: the name that will be displayed for this component
          - `room` [String]: the room where the component is
          - `type` [String]: the component type
          - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
       */
      function DomotixComponent(id, name, room, type, options) {
        if (name == null) {
          name = "";
        }
        this.id = id;
        this.name = name;
        this.room = room;
        this.type = type;
        this.options = options;
        this.status = options.defaultStatus;
      }


      /* Get the component id */

      DomotixComponent.prototype.getId = function() {
        return this.id;
      };


      /* Get the component name */

      DomotixComponent.prototype.getName = function() {
        return this.name;
      };


      /* Get the component room */

      DomotixComponent.prototype.getRoom = function() {
        return this.room;
      };


      /* Get the component type */

      DomotixComponent.prototype.getType = function() {
        return this.type;
      };


      /* Get the component status */

      DomotixComponent.prototype.getStatus = function() {
        return this.status;
      };


      /*
          Set the component status
      
          - `s` [String]: the new status
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixComponent.prototype.setStatus = function(s, cb) {
        this.status = s;
        return cb(this);
      };

      return DomotixComponent;

    })();

    /* Return the class */
    return DomotixComponent;
  });

}).call(this);
