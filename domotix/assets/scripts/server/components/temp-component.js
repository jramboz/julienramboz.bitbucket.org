
/**
    The ligth component class that represents the light controller.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
 */


/*
    AMD loader
    Try loading as AMD module or fall back to default loading
 */

(function() {
  var domotixTempComponent,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  (function(plugin) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      return define(["jslib", "component"], plugin);
    } else {
      return plugin(this.$, this.DomotixComponent);
    }
  })(domotixTempComponent = function($, DomotixComponent) {
    "use strict";

    /* Defining the class */
    var DomotixTempComponent;
    DomotixTempComponent = (function(_super) {
      __extends(DomotixTempComponent, _super);


      /*
          Default constructor
      
          - `id` [String]: the component id
          - `name` [String]: the name that will be displayed for this component
          - `room` [String]: the room where the component is
          - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
       */

      function DomotixTempComponent(id, name, room, options) {
        if (name == null) {
          name = "";
        }
        DomotixTempComponent.__super__.constructor.call(this, id, name, room, "temperature", $.extend({
          defaultStatus: "off",
          defaultTemp: 21,
          minTemp: 14,
          maxTemp: 28,
          dayTemp: 21,
          nightTemp: 16
        }, options));
        this.temp = this.options.defaultTemp;
      }


      /*
          Set the component status after validation
      
          - `s` [String]: the new status
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixTempComponent.prototype.setStatus = function(s, cb) {
        s = s.toLowerCase();
        if (!s.match(/^(day|night|off)$/i)) {
          return cb(null, "invalid status");
        }
        return DomotixTempComponent.__super__.setStatus.call(this, s, cb);
      };


      /*
          Set the component status to "day"
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixTempComponent.prototype.switchDayMode = function(cb) {
        return this.setTemp(this.options.dayTemp, (function(_this) {
          return function() {
            return _this.setStatus("day", cb);
          };
        })(this));
      };


      /*
          Set the component status to "night"
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixTempComponent.prototype.switchNightMode = function(cb) {
        return this.setTemp(this.options.nightTemp, (function(_this) {
          return function() {
            return _this.setStatus("night", cb);
          };
        })(this));
      };


      /*
          Set the component status to "off"
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixTempComponent.prototype.switchOff = function(cb) {
        return this.setStatus("off", cb);
      };


      /*
          Adjust the desired temperature
      
          - `i` [int]: the new intensity (0 <= `i` <= 100)
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixTempComponent.prototype.setTemp = function(t, cb) {
        t = parseInt(t, 10);
        if (isNaN(t)) {
          return cb(null, "invalid temperature");
        }
        this.temp = t;
        return cb(this);
      };


      /* Get the desired temperature */

      DomotixTempComponent.prototype.getTemp = function() {
        return this.temp;
      };


      /* Get the current temperature */

      DomotixTempComponent.prototype.getCurrentTemp = function() {

        /* Assume the actual sensor is returning 19°C for this test case */
        return 19;
      };


      /*
          Increase the temperature
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixTempComponent.prototype.increaseTemp = function(cb) {
        return this.setTemp(Math.min(this.temp + 1, this.options.maxTemp), cb);
      };


      /*
          Decrease the temperature
      
          - `cb` [Function]: the callback function to call (will get the updated component as parameter)
       */

      DomotixTempComponent.prototype.decreaseTemp = function(cb) {
        return this.setTemp(Math.max(this.temp - 1, this.options.minTemp), cb);
      };

      return DomotixTempComponent;

    })(DomotixComponent);

    /* Return the class */
    return DomotixTempComponent;
  });

}).call(this);
